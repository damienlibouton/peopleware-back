package com.peopleware.recruitment.application;

public class DegreeDTO {
	public String name;

	@Override
	public String toString() {
		return "DegreeDTO [" + (name != null ? "name=" + name : "") + "]";
	}
}
