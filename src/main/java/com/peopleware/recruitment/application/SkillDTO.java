package com.peopleware.recruitment.application;

public class SkillDTO {
	public String name;
	public int knowledgeLevel;
	
	@Override
	public String toString() {
		return "SkillDTO [" + (name != null ? "name=" + name + ", " : "") + "knowledgeLevel=" + knowledgeLevel + "]";
	}
	
}
