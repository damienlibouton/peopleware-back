package com.peopleware.recruitment.application;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import com.peopleware.recruitment.domain.applicant.Applicant;
import com.peopleware.recruitment.domain.applicant.ApplicantDegree;
import com.peopleware.recruitment.domain.applicant.ApplicantSkill;
import com.peopleware.recruitment.domain.applicant.IApplicantRepository;
import com.peopleware.recruitment.domain.company.ICompanyRepository;
import com.peopleware.recruitment.domain.company.JobOffer;
import com.peopleware.recruitment.domain.company.RequiredAcademicDegree;
import com.peopleware.recruitment.domain.company.RequiredSkill;

public class ApplicantCommandsAndQueries {
	private final ICompanyRepository companyRepository;
	private final IApplicantRepository applicantRepository;

	public ApplicantCommandsAndQueries(ICompanyRepository companyRepository,
			IApplicantRepository applicantRepository) {
		this.companyRepository = companyRepository;
		this.applicantRepository = applicantRepository;
	}
	
	////Commands section
	public void postApplicant(ApplicantDTO applicantDTO) {
		Applicant applicantToAdd;

		applicantToAdd = new Applicant(applicantDTO.fullName, applicantDTO.contactNumber, applicantDTO.email,
				applicantDTO.monthlyMinSalary, applicantDTO.workingTime, mapDegreesDTOToApplicantDegrees(applicantDTO.degrees),
				mapSkillsDTOToApplicantSkills(applicantDTO.skills));

		applicantRepository.AddApplicant(applicantToAdd);
		applicantDTO.id = applicantToAdd.getId();
	}
	
	////Queries section
	public ApplicantDTO getApplicant(long applicantId) {
		return mapApplicantToDTO(applicantRepository.findApplicantById(applicantId));
	}
	
	public List<ApplicantDTO> getAllApplicants() {
		return mapApplicantsToDTO(applicantRepository.findAllApplicants());
	}
	
	public List<ApplicantDTO> getApplicantsForAJobOffer(long jobId) {
		JobOffer job = companyRepository.findJobOfferById(jobId);
		
		List<String> degrees = new ArrayList<>();
		for (RequiredAcademicDegree degree : job.getRequiredAcademicDegrees()) {
			degrees.add(degree.getName());
		}
		
		int knowledgeLevelOfRequiredSkills = 0;
		List<String> skills = new ArrayList<>();
		for (RequiredSkill skill : job.getRequiredSkills()) {
			skills.add(skill.getName());
			knowledgeLevelOfRequiredSkills += skill.getKnowledgeLevel();
		}

		List<Applicant> applicants = applicantRepository
				.findApplicantsWhithDegreesAndSkills(degrees, skills, knowledgeLevelOfRequiredSkills);
		
		int monthlyMaxSalary = (int) (job.getYearlySalaryRange().getMaxYearlySalary() / 12);
		List<ApplicantDTO> applicantsDTO = new ArrayList<>();
		for (Applicant applicant : applicants) {
			if (applicant.getMonthlyMinSalary() <= monthlyMaxSalary) {
			ApplicantDTO applicantDTO = mapApplicantToDTOWithKnowledgeLevelOfRequiredSkills(applicant,
					applicant.getKnowledgeLevelOfRequiredSkills(
							mapRequiredSkillsToApplicantSkills(job.getRequiredSkills())));
			applicantsDTO.add(applicantDTO);
			}
		}

		return applicantsDTO;
	}
	
	////Private methods
	private ApplicantSkill mapRequiredSkillToApplicantSkill(RequiredSkill requiredSkill) {
		return new ApplicantSkill(requiredSkill.getName(), requiredSkill.getKnowledgeLevel());
	}

	private List<ApplicantSkill> mapRequiredSkillsToApplicantSkills(List<RequiredSkill> requiredSkills) {
		List<ApplicantSkill> skills = new LinkedList<>();

		for (RequiredSkill skill : requiredSkills) {
			ApplicantSkill applicantSkill = mapRequiredSkillToApplicantSkill(skill);
			skills.add(applicantSkill);
		}

		return skills;
	}
	
	private DegreeDTO mapApplicantDegreeToDTO(ApplicantDegree applicantDegree) {
		DegreeDTO degreeDTO = new DegreeDTO();
		degreeDTO.name = applicantDegree.getName();
		
		return degreeDTO;
	}
	
	private SkillDTO mapApplicantSkillToDTO(ApplicantSkill applicantSkill) {
		SkillDTO skillDTO = new SkillDTO();
		skillDTO.name = applicantSkill.getName();
		skillDTO.knowledgeLevel = applicantSkill.getKnowledgeLevel();
		
		return skillDTO;
	}
	
	private ApplicantDTO mapApplicantToDTO(Applicant applicant) {
		ApplicantDTO applicantDTO = new ApplicantDTO();
		
		applicantDTO.id = applicant.getId();
		applicantDTO.fullName = applicant.getFullName();
		applicantDTO.contactNumber = applicant.getContactNumber();
		applicantDTO.email = applicant.getEmail();
		applicantDTO.monthlyMinSalary = applicant.getMonthlyMinSalary();
		applicantDTO.workingTime = applicant.getWorkingTime().name();
		
		applicantDTO.degrees = new LinkedList<>();
		for (ApplicantDegree applicantDegree : applicant.getAcademicDegrees()) {
			DegreeDTO degreeDTO = mapApplicantDegreeToDTO(applicantDegree);
			applicantDTO.degrees.add(degreeDTO);
		}
		
		applicantDTO.skills = new LinkedList<>();
		for (ApplicantSkill applicantSkill : applicant.getSkills()) {
			SkillDTO skillDTO = mapApplicantSkillToDTO(applicantSkill);
			applicantDTO.skills.add(skillDTO);
		}
		
		return applicantDTO;
	}
	
	private ApplicantDTO mapApplicantToDTOWithKnowledgeLevelOfRequiredSkills(Applicant applicant, int knowledgeLevel) {
		ApplicantDTO applicantDTO = mapApplicantToDTO(applicant);
		applicantDTO.knowledgeLevelOfRequiredSkills = knowledgeLevel;
		
		return applicantDTO;
	}
	
	private List<ApplicantDTO> mapApplicantsToDTO(List<Applicant> applicants) {
		List<ApplicantDTO> applicantsDTO = new ArrayList<>();
		
		for (Applicant applicant : applicants) {
			ApplicantDTO applicantDTO = mapApplicantToDTO(applicant);
			applicantsDTO.add(applicantDTO);
		}
		
		return applicantsDTO;
	}
	
	private ApplicantDegree mapDegreeDTOToApplicantDegree(DegreeDTO degreeDTO) {
		return new ApplicantDegree(degreeDTO.name);
	}
	
	private List<ApplicantDegree> mapDegreesDTOToApplicantDegrees(List<DegreeDTO> degreesDTO) {
		List<ApplicantDegree> degrees = new LinkedList<>();
		
		for (DegreeDTO degreeDTO : degreesDTO) {
			ApplicantDegree applicantDegree = mapDegreeDTOToApplicantDegree(degreeDTO);
			degrees.add(applicantDegree);
		}
		
		return degrees;
	}
	
	private ApplicantSkill mapSkillDTOToApplicantSkill(SkillDTO skillDTO) {
		return new ApplicantSkill(skillDTO.name, skillDTO.knowledgeLevel);
	}
	
	private List<ApplicantSkill> mapSkillsDTOToApplicantSkills(List<SkillDTO> skillsDTO) {
		List<ApplicantSkill> skills = new LinkedList<>();
		
		for (SkillDTO skillDTO : skillsDTO) {
			ApplicantSkill applicantSkill = mapSkillDTOToApplicantSkill(skillDTO);
			skills.add(applicantSkill);
		}
		
		return skills;
	}
}
