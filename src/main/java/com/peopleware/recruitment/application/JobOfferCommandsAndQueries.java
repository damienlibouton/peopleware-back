package com.peopleware.recruitment.application;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import com.peopleware.recruitment.domain.applicant.ApplicantDegree;
import com.peopleware.recruitment.domain.applicant.Applicant;
import com.peopleware.recruitment.domain.applicant.IApplicantRepository;
import com.peopleware.recruitment.domain.applicant.ApplicantSkill;
import com.peopleware.recruitment.domain.company.Company;
import com.peopleware.recruitment.domain.company.ICompanyRepository;
import com.peopleware.recruitment.domain.company.JobOffer;
import com.peopleware.recruitment.domain.company.RequiredAcademicDegree;
import com.peopleware.recruitment.domain.company.RequiredSkill;

public class JobOfferCommandsAndQueries {
	private final ICompanyRepository companyRepository;
	private final IApplicantRepository applicantRepository;
	
	public JobOfferCommandsAndQueries(ICompanyRepository companyRepository,
			IApplicantRepository applicantRepository) {
		this.companyRepository = companyRepository;
		this.applicantRepository = applicantRepository;
	}
	
	////Commands section
	public void postJobOffer(JobDTO jobOfferDTO) {
		JobOffer jobToAdd;
		Company company = companyRepository.findCompanyById(jobOfferDTO.companyId);

		jobToAdd = new JobOffer(jobOfferDTO.name, jobOfferDTO.description, jobOfferDTO.minYearlySalary, jobOfferDTO.maxYearlySalary,
				jobOfferDTO.workingTime, mapDegreesDTOToRequiredAcademicDegrees(jobOfferDTO.degrees),
				mapSkillsDTOToRequiredSkills(jobOfferDTO.skills), company);

		companyRepository.AddJobOffer(jobToAdd);
		jobOfferDTO.id = jobToAdd.getId();
	}
	
	////Queries section
	public JobDTO getJobOffer(long jobId) {
		return mapJobOfferToDTO(companyRepository.findJobOfferById(jobId));
	}
	
	public List<JobDTO> getAllJobOffers() {
		return mapJobOffersToDTO(companyRepository.findAllJobOffers());
	}
	
	public List<JobDTO> getAllJobOffersForACompany(long companyId) {
		return mapJobOffersToDTO(companyRepository.findCompanyJobOffers(companyId));
	}
	
	public CompanyDTO getCompany(long companyId) {
		return mapCompanyToDTO(companyRepository.findCompanyById(companyId));
	}
	
	public List<CompanyDTO> getCompanies() {
		return mapCompaniesToDTO(companyRepository.findAllCompanies());
	}
	
	public List<JobDTO> getJobsMatchingDegreesAndSkillsOfApplicant(long applicantId) {
		Applicant applicant = applicantRepository.findApplicantById(applicantId);
		
		List<String> degrees = new ArrayList<>();
		for (ApplicantDegree degree : applicant.getAcademicDegrees()) {
			degrees.add(degree.getName());
		}
		
		List<String> skills = new ArrayList<>();
		for (ApplicantSkill skill : applicant.getSkills()) {
			skills.add(skill.getName());
		}
		int yearlyMinSalary = applicant.getMonthlyMinSalary()*12;
		List<JobOffer> jobs = companyRepository.findJobOffersWhithDegreesAndSkills(
				applicantId, degrees, skills);
		List<JobOffer> jobsWithYearlyMinSalary = new LinkedList<>();
		for (JobOffer job : jobs) {
			if (job.getYearlySalaryRange().getMaxYearlySalary() >= yearlyMinSalary) {
				jobsWithYearlyMinSalary.add(job);
			}
		}
		
		List<JobDTO> jobsDTO = mapJobOffersToDTO(jobsWithYearlyMinSalary);
		
		return jobsDTO;
	}
	
	////Private methods
	private RequiredAcademicDegree mapDegreeDTOToRequiredAcademicDegree(DegreeDTO degree) {
		return new RequiredAcademicDegree(degree.name);
	}
	
	private List<RequiredAcademicDegree> mapDegreesDTOToRequiredAcademicDegrees(List<DegreeDTO> degreesDTO) {
		List<RequiredAcademicDegree> degrees = new LinkedList<>();
		
		for (DegreeDTO degree : degreesDTO) {
			RequiredAcademicDegree reqDegree = mapDegreeDTOToRequiredAcademicDegree(degree);
			degrees.add(reqDegree);
		}
		
		return degrees;
	}
	
	private RequiredSkill mapSkillDTOToRequiredSkill(SkillDTO skill) {
		return new RequiredSkill(skill.name, skill.knowledgeLevel);
	}
	
	private List<RequiredSkill> mapSkillsDTOToRequiredSkills(List<SkillDTO> skillsDTO) {
		List<RequiredSkill> skills = new LinkedList<>();
		
		for (SkillDTO skill : skillsDTO) {
			RequiredSkill reqSkill = mapSkillDTOToRequiredSkill(skill);
			skills.add(reqSkill);
		}
		
		return skills;
	}
	
	private DegreeDTO mapRequiredAcademicDegreeToDTO(RequiredAcademicDegree requiredDegree) {
		DegreeDTO degreeDTO = new DegreeDTO();
		degreeDTO.name = requiredDegree.getName();
		
		return degreeDTO;
	}
	
	private SkillDTO mapRequiredSkillToDTO(RequiredSkill requiredSkill) {
		SkillDTO skillDTO = new SkillDTO();
		skillDTO.name = requiredSkill.getName();
		skillDTO.knowledgeLevel = requiredSkill.getKnowledgeLevel();
		
		return skillDTO;
	}
	
	private JobDTO mapJobOfferToDTO(JobOffer job) {
		JobDTO jobDTO = new JobDTO();
		
		jobDTO.id = job.getId();
		jobDTO.name = job.getName();
		jobDTO.description = job.getDescription();
		jobDTO.minYearlySalary = job.getYearlySalaryRange().getMinYearlySalary();
		jobDTO.maxYearlySalary = job.getYearlySalaryRange().getMaxYearlySalary();
		jobDTO.workingTime = job.getWorkingTime().name();
		
		jobDTO.degrees = new LinkedList<>();
		for (RequiredAcademicDegree requiredDegree : job.getRequiredAcademicDegrees()) {
			DegreeDTO degreeDTO = mapRequiredAcademicDegreeToDTO(requiredDegree);
			jobDTO.degrees.add(degreeDTO);
		}
		
		jobDTO.skills = new LinkedList<>();
		for (RequiredSkill requiredSkill : job.getRequiredSkills()) {
			SkillDTO skillDTO = mapRequiredSkillToDTO(requiredSkill);
			jobDTO.skills.add(skillDTO);
		}
		
		jobDTO.companyId = job.getCompany().getId();
		jobDTO.companyName = job.getCompany().getName();
		jobDTO.companyContactNumber = job.getCompany().getContactNumber();
		
		return jobDTO;
	}
	
	private List<JobDTO> mapJobOffersToDTO(List<JobOffer> jobOffers) {
		List<JobDTO> jobsDTO = new LinkedList<>();
		
		for (JobOffer jobOffer : jobOffers) {
			JobDTO jobDTO = mapJobOfferToDTO(jobOffer);
			jobsDTO.add(jobDTO);
		}
		
		return jobsDTO;
	}
	
	private CompanyDTO mapCompanyToDTO(Company company) {
		CompanyDTO companyDTO = new CompanyDTO();
		
		companyDTO.id = company.getId();
		companyDTO.name = company.getName();
		companyDTO.contactNumber = company.getContactNumber();
		List<JobDTO> jobsDTO = mapJobOffersToDTO(company.getJobs());
		companyDTO.jobs = jobsDTO;
		
		return companyDTO;
	}
	
	private List<CompanyDTO> mapCompaniesToDTO(List<Company> companies) {
		List<CompanyDTO> companiesDTO = new LinkedList<>();
		
		for (Company company : companies) {
			CompanyDTO companyDTO = mapCompanyToDTO(company);
			companiesDTO.add(companyDTO);
		}
		
		return companiesDTO;
	}
}
