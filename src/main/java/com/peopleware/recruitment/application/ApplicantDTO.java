package com.peopleware.recruitment.application;

import java.util.List;

public class ApplicantDTO {
	public long id;
	public String fullName;
	public String contactNumber;
	public String email;
	public int monthlyMinSalary;
	public List<DegreeDTO> degrees;
	public List<SkillDTO> skills;
	public String workingTime;
	public int knowledgeLevelOfRequiredSkills;
	
	@Override
	public String toString() {
		return "ApplicantDTO [id=" + id + ", " + (fullName != null ? "fullName=" + fullName + ", " : "")
				+ (contactNumber != null ? "contactNumber=" + contactNumber + ", " : "")
				+ (email != null ? "email=" + email + ", " : "") + "monthlyMinSalary=" + monthlyMinSalary + ", "
				+ (degrees != null ? "degrees=" + degrees + ", " : "")
				+ (skills != null ? "skills=" + skills + ", " : "")
				+ (workingTime != null ? "workingTime=" + workingTime + ", " : "") + "knowledgeLevelOfRequiredSkillss="
				+ knowledgeLevelOfRequiredSkills + "]";
	}
}
