package com.peopleware.recruitment.application;

import java.util.List;

public class JobDTO {
	public long id;
	public String name;
	public String description;
	public int minYearlySalary;
	public int maxYearlySalary;
	public String workingTime;
	public List<DegreeDTO> degrees;
	public List<SkillDTO> skills;
	public long companyId;
	public String companyName;
	public String companyContactNumber;
	
	@Override
	public String toString() {
		return "JobDTO [id=" + id + ", " + (name != null ? "name=" + name + ", " : "")
				+ (description != null ? "description=" + description + ", " : "") + "minYearlySalary="
				+ minYearlySalary + ", maxYearlySalary=" + maxYearlySalary + ", "
				+ (workingTime != null ? "workingTime=" + workingTime + ", " : "")
				+ (degrees != null ? "degrees=" + degrees + ", " : "")
				+ (skills != null ? "skills=" + skills + ", " : "") + "companyId=" + companyId + ", "
				+ (companyName != null ? "companyName=" + companyName + ", " : "")
				+ (companyContactNumber != null ? "companyContactNumber=" + companyContactNumber : "") + "]";
	}
}
