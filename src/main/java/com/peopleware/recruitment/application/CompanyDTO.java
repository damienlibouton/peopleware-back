package com.peopleware.recruitment.application;

import java.util.List;

public class CompanyDTO {
	public long id;
	public String name;
	public String contactNumber;
	public List<JobDTO> jobs;
	
	@Override
	public String toString() {
		return "CompanyDTO [id=" + id + ", " + (name != null ? "name=" + name + ", " : "")
				+ (contactNumber != null ? "contactNumber=" + contactNumber + ", " : "")
				+ (jobs != null ? "jobs=" + jobs : "") + "]";
	}
}
