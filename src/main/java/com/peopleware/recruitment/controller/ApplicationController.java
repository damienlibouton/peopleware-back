package com.peopleware.recruitment.controller;

import java.util.List;

import org.apache.commons.lang3.exception.ExceptionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

import com.peopleware.recruitment.application.ApplicantCommandsAndQueries;
import com.peopleware.recruitment.application.ApplicantDTO;
import com.peopleware.recruitment.application.CompanyDTO;
import com.peopleware.recruitment.application.JobDTO;
import com.peopleware.recruitment.application.JobOfferCommandsAndQueries;

@RestController
@RequestMapping("/api")
public class ApplicationController {
	
	public static final Logger logger = LoggerFactory.getLogger(ApplicationController.class);
	 
    @Autowired
    ApplicantCommandsAndQueries applicantCQ;
    
    @Autowired
    JobOfferCommandsAndQueries jobCQ;
    
    /************************APPLICANT ROUTES************************/
    @CrossOrigin(origins = "http://localhost:4200")
    @RequestMapping(value = "/applicant/", method = RequestMethod.GET)
    public ResponseEntity<List<ApplicantDTO>> listAllApplicants() {
    	String message = "Fetching all Applicants";
    	logger.info(message);
    	
    	List<ApplicantDTO> applicants = null;
    	
    	try {
    		applicants = applicantCQ.getAllApplicants();
    	} catch (Exception e) {
    		String stack = ExceptionUtils.getStackTrace(e);
    		logger.error(message + " ERROR :\n" + stack);
    		return new ResponseEntity<List<ApplicantDTO>>(HttpStatus.NOT_FOUND);
    	}
        
        if (applicants.isEmpty()) {
            return new ResponseEntity<List<ApplicantDTO>>(HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<List<ApplicantDTO>>(applicants, HttpStatus.OK);
    }
 
    @CrossOrigin(origins = "http://localhost:4200")
    @RequestMapping(value = "/applicant/{id}", method = RequestMethod.GET)
    public ResponseEntity<ApplicantDTO> getApplicant(@PathVariable("id") long id) {
    	String message = "Fetching Applicant with id " + id;
    	logger.info(message);
    	
    	ApplicantDTO applicant = null;
    	
    	try {
    		applicant = applicantCQ.getApplicant(id);
    	} catch (Exception e) {
    		String stack = ExceptionUtils.getStackTrace(e);
    		logger.error(message + " ERROR :\n" + stack);
    		return new ResponseEntity<ApplicantDTO>(HttpStatus.NOT_FOUND);
    	}
        
        if (applicant == null) {
            return new ResponseEntity<ApplicantDTO>(HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<ApplicantDTO>(applicant, HttpStatus.OK);
    }
    
    @CrossOrigin(origins = "http://localhost:4200")
    @RequestMapping(value = "/applicant/{applicantId}/job/", method = RequestMethod.GET)
    public ResponseEntity<List<JobDTO>> getJobsMatchingApplicant(@PathVariable("applicantId") long applicantId) {
    	String message = "Fetching jobs for Applicant with id " + applicantId;
    	logger.info(message);
    	
    	List<JobDTO> jobs = null;
    	
    	try {
    		jobs = jobCQ.getJobsMatchingDegreesAndSkillsOfApplicant(applicantId);
    	} catch (Exception e) {
    		String stack = ExceptionUtils.getStackTrace(e);
    		logger.error(message + " ERROR :\n" + stack);
    		return new ResponseEntity<List<JobDTO>>(HttpStatus.NOT_FOUND);
    	}
        
        if (jobs.isEmpty()) {
            return new ResponseEntity<List<JobDTO>>(HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<List<JobDTO>>(jobs, HttpStatus.OK);
    }
    
    
    @CrossOrigin(origins = "http://localhost:4200")
    @RequestMapping(value = "/applicant/", method = RequestMethod.POST)
    public ResponseEntity<String> addApplicant(@RequestBody ApplicantDTO applicant, UriComponentsBuilder ucBuilder) {
    	String message = "Creating Applicant :\n" + applicant.toString();
    	logger.info(message);
    	
    	try {
    		applicantCQ.postApplicant(applicant);
    	} catch (Exception e) {
    		String stack = ExceptionUtils.getStackTrace(e);
    		logger.error(message + " ERROR :\n" + stack);
    		return new ResponseEntity<String>(HttpStatus.CONFLICT);
    	}
    	
    	logger.info("Applicant created with id : " + applicant.id);
    	
        HttpHeaders headers = new HttpHeaders();
        headers.setLocation(ucBuilder.path("/api/applicant/{id}").buildAndExpand(applicant.id).toUri());
        return new ResponseEntity<String>(headers, HttpStatus.CREATED);
    }
    
    /************************COMPANY ROUTES************************/
    
    @CrossOrigin(origins = "http://localhost:4200")
    @RequestMapping(value = "/company/", method = RequestMethod.GET)
    public ResponseEntity<List<CompanyDTO>> listAllCompanies() {
    	String message = "Fetching all Companies";
    	logger.info(message);
    	
    	List<CompanyDTO> companies = null;
    	
    	try {
    		companies = jobCQ.getCompanies();
    	} catch (Exception e) {
    		String stack = ExceptionUtils.getStackTrace(e);
    		logger.error(message + " ERROR :\n" + stack);
    		return new ResponseEntity<List<CompanyDTO>>(HttpStatus.NOT_FOUND);
    	}
        
        if (companies.isEmpty()) {
            return new ResponseEntity<List<CompanyDTO>>(HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<List<CompanyDTO>>(companies, HttpStatus.OK);
    }
    
    @CrossOrigin(origins = "http://localhost:4200")
    @RequestMapping(value = "/company/{id}", method = RequestMethod.GET)
    public ResponseEntity<CompanyDTO> getCompany(@PathVariable("id") long id) {
    	String message = "Fetching Company with id " + id;
    	logger.info(message);
    	
    	CompanyDTO company = null;
    	
    	try {
    		company = jobCQ.getCompany(id);
    	} catch (Exception e) {
    		String stack = ExceptionUtils.getStackTrace(e);
    		logger.error(message + " ERROR :\n" + stack);
    		return new ResponseEntity<CompanyDTO>(HttpStatus.NOT_FOUND);
    	}
        
        if (company == null) {
            return new ResponseEntity<CompanyDTO>(HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<CompanyDTO>(company, HttpStatus.OK);
    }
    
    @CrossOrigin(origins = "http://localhost:4200")
    @RequestMapping(value = "/company/{companyId}/job/", method = RequestMethod.POST)
    public ResponseEntity<String> addJobOffer(@PathVariable("companyId") long companyId, @RequestBody JobDTO job, UriComponentsBuilder ucBuilder) {
    	String message = "Add job to Company with id " + companyId + " :\n" + job.toString();
    	logger.info(message);
    	
    	try {
    		jobCQ.postJobOffer(job);
    	} catch (Exception e) {
    		String stack = ExceptionUtils.getStackTrace(e);
    		logger.error(message + " ERROR :\n" + stack);
    		return new ResponseEntity<String>(HttpStatus.CONFLICT);
    	}
    	
    	logger.info("Job created with id : " + job.id);
    	
        HttpHeaders headers = new HttpHeaders();
        headers.setLocation(ucBuilder.path("/api/job/{id}").buildAndExpand(job.id).toUri());
        return new ResponseEntity<String>(headers, HttpStatus.CREATED);
    }
    
    /************************JOB OFFERS ROUTES************************/
    
    @CrossOrigin(origins = "http://localhost:4200")
    @RequestMapping(value = "/job/", method = RequestMethod.GET)
    public ResponseEntity<List<JobDTO>> listAllJobOffers() {
    	String message = "Fetching all job offers";
    	logger.info(message);
    	
    	List<JobDTO> jobs = null;
    	
    	try {
    		jobs = jobCQ.getAllJobOffers();
    	} catch (Exception e) {
    		String stack = ExceptionUtils.getStackTrace(e);
    		logger.error(message + " ERROR :\n" + stack);
    		return new ResponseEntity<List<JobDTO>>(HttpStatus.NOT_FOUND);
    	}
        
        if (jobs.isEmpty()) {
            return new ResponseEntity<List<JobDTO>>(HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<List<JobDTO>>(jobs, HttpStatus.OK);
    }
    
    @CrossOrigin(origins = "http://localhost:4200")
    @RequestMapping(value = "/job/{id}", method = RequestMethod.GET)
    public ResponseEntity<JobDTO> getJobOffer(@PathVariable("id") long id) {
    	String message = "Fetching job offer with id " + id;
    	logger.info(message);
    	
    	JobDTO job = null;
    	
    	try {
    		job = jobCQ.getJobOffer(id);
    	} catch (Exception e) {
    		String stack = ExceptionUtils.getStackTrace(e);
    		logger.error(message + " ERROR :\n" + stack);
    		return new ResponseEntity<JobDTO>(HttpStatus.NOT_FOUND);
    	}
        
        if (job == null) {
            return new ResponseEntity<JobDTO>(HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<JobDTO>(job, HttpStatus.OK);
    }
    
    @CrossOrigin(origins = "http://localhost:4200")
    @RequestMapping(value = "/job/{jobId}/applicant/", method = RequestMethod.GET)
    public ResponseEntity<List<ApplicantDTO>> getApplicantsMatchingJob(@PathVariable("jobId") long jobId) {
    	String message = "Fetching applicants for job offer with id " + jobId;
    	logger.info(message);
    	
    	List<ApplicantDTO> applicants = null;
    	
    	try {
    		applicants = applicantCQ.getApplicantsForAJobOffer(jobId);
    	} catch (Exception e) {
    		String stack = ExceptionUtils.getStackTrace(e);
    		logger.error(message + " ERROR :\n" + stack);
    		return new ResponseEntity<List<ApplicantDTO>>(HttpStatus.NOT_FOUND);
    	}
        
        if (applicants.isEmpty()) {
            return new ResponseEntity<List<ApplicantDTO>>(HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<List<ApplicantDTO>>(applicants, HttpStatus.OK);
    }

}
