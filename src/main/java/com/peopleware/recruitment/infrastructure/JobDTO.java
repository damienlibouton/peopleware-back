package com.peopleware.recruitment.infrastructure;

public class JobDTO {
	long id;
	String name;
	String description;
	int minYearlySalary;
	int maxYearlySalary;
	int workingTime;
	long companyId;
}
