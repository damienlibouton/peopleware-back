package com.peopleware.recruitment.infrastructure;

import java.util.LinkedList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.sql2o.Connection;
import org.sql2o.Query;
import org.sql2o.Sql2o;
import org.sql2o.Sql2oException;

import com.peopleware.recruitment.domain.applicant.Applicant;
import com.peopleware.recruitment.domain.applicant.ApplicantDegree;
import com.peopleware.recruitment.domain.applicant.ApplicantSkill;
import com.peopleware.recruitment.domain.applicant.IApplicantRepository;
import com.peopleware.recruitment.domain.common.WorkingTime;

public class ApplicantRepositoryH2 implements IApplicantRepository{
	private final Sql2o sql2o;
	private final Logger logger = LoggerFactory.getLogger(ApplicantRepositoryH2.class);

	public ApplicantRepositoryH2(Sql2o sql2o) {
		this.sql2o = sql2o;
	}
	
	@Override
	public List<Applicant> findAllApplicants() {
		
		List<ApplicantDTO> dtos = findApplicantsDTO();
		List<Applicant> applicants = new LinkedList<>();
		
		for (ApplicantDTO dto: dtos) {
			Applicant app = mapDTOToApplicant(dto);
			applicants.add(app);
		}
		
		return dtos != null ? applicants : null;
	}

	@Override
	public Applicant findApplicantById(long applicantId) {

		ApplicantDTO dto = findApplicantDTOById(applicantId);
		
		return dto != null ? mapDTOToApplicant(dto) : null;
	}

	@Override
	public List<Applicant> findApplicantsWhithDegreesAndSkills(List<String> requiredDegreesName,
			List<String> requiredskillsName, int knowledgeLevelOfReuiredSkills) {
		
		List<ApplicantDTO> dtos = findApplicantsDTOWhithDegreesAndSkills(requiredDegreesName, requiredskillsName, knowledgeLevelOfReuiredSkills);
		List<Applicant> applicants = new LinkedList<>();
		
		for (ApplicantDTO dto: dtos) {
			Applicant app = mapDTOToApplicant(dto);
			applicants.add(app);
		}
		
		return dtos != null ? applicants : null;
	}

	@Override
	public void AddApplicant(Applicant applicant) {
		String sqlInsertApplicant = "insert into Applicants ("
				+ "fullName, "
				+ "contactNumber, "
				+ "email, "
				+ "monthlyMinSalary, "
				+ "workingTime, "
				+ ") values ( "
				+ ":fullName, "
				+ ":contactNumber, "
				+ ":email, "
				+ ":monthlyMinSalary, "
				+ ":workingTime )";
		
		String sqlInsertDegree = "insert into ApplicantDegrees ("
				+ "name, "
				+ "applicantId "
				+ ") values ( "
				+ ":name, "
				+ ":applicantId )";
		
		String sqlInsertSkill = "insert into ApplicantSkills ("
				+ "name, "
				+ "knowledgeLevel, "
				+ "applicantId "
				+ ") values ( "
				+ ":name, "
				+ ":knowledgeLevel, "
				+ ":applicantId )";
		
		try (Connection con = sql2o.beginTransaction()) {
		    long insertedId = (long) con.createQuery(sqlInsertApplicant, true)
			    .addParameter("fullName", applicant.getFullName())
			    .addParameter("contactNumber", applicant.getContactNumber())
			    .addParameter("email", applicant.getEmail())
			    .addParameter("monthlyMinSalary", applicant.getMonthlyMinSalary())
			    .addParameter("workingTime", applicant.getWorkingTime().getCode())
			    .executeUpdate()
			    .getKey();
		    
		    applicant.setId(insertedId);
		    
		    Query queryForDegrees = con.createQuery(sqlInsertDegree);
		    applicant.getAcademicDegrees().forEach(d -> queryForDegrees
		    		.addParameter("name", d.getName())
		    		.addParameter("applicantId", insertedId)
		    		.addToBatch()
		    		);
		    queryForDegrees.executeBatch();
		    
		    Query queryForSkills = con.createQuery(sqlInsertSkill);
		    applicant.getSkills().forEach(s -> queryForSkills
		    		.addParameter("name", s.getName())
		    		.addParameter("knowledgeLevel", s.getKnowledgeLevel())
		    		.addParameter("applicantId", insertedId)
		    		.addToBatch()
		    		);
		    queryForSkills.executeBatch();
		    
		    con.commit();
		    
		} catch (Sql2oException ex) {
			logger.error(ex.getMessage());
		}
	}
	
	private List<ApplicantDTO> findApplicantsDTO() {
		String sql = "SELECT id, fullName, contactNumber, "
				+ "email, monthlyMinSalary, workingTime " 
				+ "FROM Applicants ";

		try (Connection con = sql2o.open()) {
			return con.createQuery(sql)
					.executeAndFetch(ApplicantDTO.class);
		}
		catch (Sql2oException ex) {
			logger.error(ex.getMessage());
			return null;
		}
	}

	private ApplicantDTO findApplicantDTOById(long applicantId) {
		String sql = "SELECT id, fullName, contactNumber, "
				+ "email, monthlyMinSalary, workingTime " 
				+ "FROM Applicants " 
				+ "WHERE id = :applicantId";

		try (Connection con = sql2o.open()) {
			return con.createQuery(sql)
					.addParameter("applicantId", applicantId)
					.executeAndFetchFirst(ApplicantDTO.class);
		}
		catch (Sql2oException ex) {
			logger.error(ex.getMessage());
			return null;
		}
	}
	
	private List<DegreeDTO> findApplicantDegreesDTO(long applicantId) {
		String sql = "SELECT name " 
				+ "FROM ApplicantDegrees "
				+ "WHERE applicantId = :applicantId ";

		try (Connection con = sql2o.open()) {
			return con.createQuery(sql)
					.addParameter("applicantId", applicantId)
					.executeAndFetch(DegreeDTO.class);
		}
		catch (Sql2oException ex) {
			logger.error(ex.getMessage());
			return null;
		}
	}
	
	private List<SkillDTO> findApplicantSkillsDTO(long applicantId) {
		String sql = "SELECT name, knowledgeLevel " 
				+ "FROM ApplicantSkills "
				+ "WHERE applicantId = :applicantId ";

		try (Connection con = sql2o.open()) {
			return con.createQuery(sql)
					.addParameter("applicantId", applicantId)
					.executeAndFetch(SkillDTO.class);
		}
		catch (Sql2oException ex) {
			logger.error(ex.getMessage());
			return null;
		}
	}
	
	private List<ApplicantDTO> findApplicantsDTOWhithDegreesAndSkills(List<String> requiredDegreesName,
			List<String> requiredskillsName, int knowledgeLevelOfRequiredSkills) {
		String sql = "SELECT app.id, app.fullName, app.contactNumber, app.email, \n" + 
				"       app.monthlyMinSalary, app.workingTime\n" + 
				"  FROM Applicants app \n" + 
				" WHERE app.id IN (\n" + 
				"  SELECT skilledApp.applicantId\n" + 
				"    FROM  \n" + 
				"    (\n" + 
				"      SELECT ski.applicantId, COUNT(DISTINCT ski.name) AS cnt, SUM(ski.knowledgeLevel) AS sum_kn\n" + 
				"        FROM ApplicantSkills ski\n" + 
				"       WHERE ski.name IN (:requiredskillsName)\n" + 
				"       GROUP BY ski.applicantId\n" + 
				"    ) AS skilledApp,\n" + 
				"    (\n" + 
				"      SELECT deg.applicantId, COUNT(DISTINCT deg.name) AS cnt\n" + 
				"        FROM ApplicantDegrees deg\n" + 
				"       WHERE deg.name IN (:requiredDegreesName)\n" + 
				"       GROUP BY deg.applicantId\n" + 
				"    ) AS degreedApp\n" + 
				" WHERE skilledApp.applicantId = degreedApp.applicantId\n" + 
				"   AND degreedApp.cnt = :cntRequiredDegrees \n" + 
				"   AND skilledApp.cnt = :cntRequiredSkills \n" + 
				"   AND skilledApp.sum_kn >= :sumOfRequiredKnowledge \n" + 
				" ORDER BY skilledApp.sum_kn DESC\n" + 
				" )";
		
		try (Connection con = sql2o.open()) {
			
			return con.createQuery(sql)
					.addParameter("requiredskillsName", requiredskillsName)
					.addParameter("requiredDegreesName", requiredDegreesName)
					.addParameter("cntRequiredSkills", requiredskillsName.size())
					.addParameter("cntRequiredDegrees", requiredDegreesName.size())
					.addParameter("sumOfRequiredKnowledge", knowledgeLevelOfRequiredSkills)
					.executeAndFetch(ApplicantDTO.class);
		}
		catch (Sql2oException ex) {
			logger.error(ex.getMessage());
			return null;
		}
	}
	
	private Applicant mapDTOToApplicant(ApplicantDTO dto) {
		List<DegreeDTO> degreesDTO = findApplicantDegreesDTO(dto.id);
		List<ApplicantDegree> degrees = new LinkedList<>();
		for (DegreeDTO degreeDTO : degreesDTO) {
			degrees.add(new ApplicantDegree(degreeDTO.name));
		}
		
		List<SkillDTO> skillsDTO = findApplicantSkillsDTO(dto.id);
		List<ApplicantSkill> skills = new LinkedList<>();
		for (SkillDTO skillDTO : skillsDTO) {
			skills.add(new ApplicantSkill(skillDTO.name, skillDTO.knowledgeLevel));
		}
		
		return new Applicant(
				dto.id,
				dto.fullName,
				dto.contactNumber,
				dto.email,
				dto.monthlyMinSalary,
				WorkingTime.getWorkingTimeFromCode(dto.workingTime).name(),
				degrees, 
				skills);
	}
}
