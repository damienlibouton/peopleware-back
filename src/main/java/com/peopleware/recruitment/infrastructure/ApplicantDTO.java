package com.peopleware.recruitment.infrastructure;

import java.util.List;

public class ApplicantDTO {
	long id;
	String fullName;
	String contactNumber;
	String email;
	int monthlyMinSalary;
	List<DegreeDTO> degrees;
	List<SkillDTO> skills;
	int workingTime;
	int knowledgeLevelOfRequiredSkillss;
}
