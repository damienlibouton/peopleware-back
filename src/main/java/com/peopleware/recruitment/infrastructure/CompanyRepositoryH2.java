package com.peopleware.recruitment.infrastructure;

import java.util.LinkedList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.sql2o.Connection;
import org.sql2o.Query;
import org.sql2o.Sql2o;
import org.sql2o.Sql2oException;

import com.peopleware.recruitment.domain.common.WorkingTime;
import com.peopleware.recruitment.domain.company.Company;
import com.peopleware.recruitment.domain.company.ICompanyRepository;
import com.peopleware.recruitment.domain.company.JobOffer;
import com.peopleware.recruitment.domain.company.RequiredAcademicDegree;
import com.peopleware.recruitment.domain.company.RequiredSkill;

public class CompanyRepositoryH2 implements ICompanyRepository {

	private final Sql2o sql2o;
	private final Logger logger = LoggerFactory.getLogger(CompanyRepositoryH2.class);

	public CompanyRepositoryH2(Sql2o sql2o) {
		this.sql2o = sql2o;
	}
	
	@Override
	public List<Company> findAllCompanies() {

		List<CompanyDTO> companiesDTO = findCompaniesDTO();
		List<Company> companies = new LinkedList<>();
		
		for (CompanyDTO companyDTO: companiesDTO) {
			List<JobDTO> jobsDTO = findCompanyJobOffersDTO(companyDTO.id);
			Company company = mapDTOToCompany(companyDTO, jobsDTO);
			companies.add(company);
		}
		
		return companiesDTO != null ? companies : null;
	}

	@Override
	public Company findCompanyById(long companyId) {

		CompanyDTO companyDTO = findCompanyDTOById(companyId);
		
		List<JobDTO> jobsDTO = findCompanyJobOffersDTO(companyId);
		
		Company company = mapDTOToCompany(companyDTO, jobsDTO);
		
		return companyDTO != null ? company : null;
	}

	@Override
	public List<JobOffer> findCompanyJobOffers(long companyId) {
		
		List<JobDTO> jobsDTO = findCompanyJobOffersDTO(companyId);
		
		CompanyDTO companyDTO = findCompanyDTOById(companyId);
		
		Company company = mapDTOToCompany(companyDTO, jobsDTO);

		return jobsDTO != null ? company.getJobs() : null;
	}
	
	@Override
	public JobOffer findJobOfferById(long jobId) {

		JobDTO jobDTO = findJobOfferDTO(jobId);
		CompanyDTO companyDTO = findCompanyDTOById(jobDTO.companyId);
		List<JobDTO> jobsDTO = new LinkedList<>();
		jobsDTO.add(jobDTO);
		
		Company company = mapDTOToCompany(companyDTO, jobsDTO);
		
		return jobDTO != null ? company.getJob(jobDTO.id) : null;
	}
	
	@Override
	public List<JobOffer> findAllJobOffers() {
		
		List<CompanyDTO> companiesDTO = findCompaniesDTO();
		List<JobOffer> jobOffers = new LinkedList<>();
		
		for (CompanyDTO companyDTO : companiesDTO) {
			List<JobDTO> jobsDTO = findCompanyJobOffersDTO(companyDTO.id);
			Company company = mapDTOToCompany(companyDTO, jobsDTO);

			jobOffers.addAll(company.getJobs());
		}
		
		return companiesDTO != null ? jobOffers : null;
	}

	@Override
	public List<RequiredAcademicDegree> findRequiredAcademicDegreeOfJobOffer(long jobId) {

		List<DegreeDTO> dtos = findRequiredDegreesJobOfferDTO(jobId);
		List<RequiredAcademicDegree> degrees = new LinkedList<>();
		
		for (DegreeDTO dto : dtos) {
			degrees.add(new RequiredAcademicDegree(dto.name));
		}
		
		return dtos != null ? degrees : null;
	}

	@Override
	public List<RequiredSkill> findRequiredSkillsOfJobOffer(long jobId) {

		List<SkillDTO> dtos = findRequiredSkillsJobOfferDTO(jobId);
		List<RequiredSkill> skills = new LinkedList<>();
		
		for (SkillDTO dto : dtos) {
			skills.add(new RequiredSkill(dto.name, dto.knowledgeLevel));
		}
		
		return dtos != null ? skills : null;
	}

	@Override
	public List<JobOffer> findJobOffersWhithDegreesAndSkills(long applicantId, List<String> applicantDegreesName,
			List<String> applicantSkillsName) {
		
		List<JobDTO> jobsDTO = findJobOffersDTOWhithDegreesAndSkills(applicantId, applicantDegreesName, applicantSkillsName);
		List<JobOffer> jobOffers = new LinkedList<>();
		
		for (JobDTO jobDTO : jobsDTO) {
			
			CompanyDTO companyDTO = findCompanyDTOById(jobDTO.companyId);
			List<JobDTO> jobDTOToList = new LinkedList<>();
			jobDTOToList.add(jobDTO);
			
			Company company = mapDTOToCompany(companyDTO, jobDTOToList);
			
			jobOffers.add(company.getJob(jobDTO.id));
		}
		
		return jobsDTO != null ? jobOffers : null;
	}

	@Override
	public void AddJobOffer(JobOffer jobOffer) {
		String sqlInsertJob = "insert into JobOffers ("
				+ "name, "
				+ "description, "
				+ "minYearlySalary, "
				+ "maxYearlySalary, "
				+ "workingTime, "
				+ "companyId "
				+ ") values ( "
				+ ":name, "
				+ ":description, "
				+ ":minYearlySalary, "
				+ ":maxYearlySalary, "
				+ ":workingTime,"
				+ ":companyId )";
		
		String sqlInsertDegree = "insert into RequiredAcademicDegrees ("
				+ "name, "
				+ "jobOfferId "
				+ ") values ( "
				+ ":name, "
				+ ":jobOfferId )";
		
		String sqlInsertSkill = "insert into RequiredSkills ("
				+ "name, "
				+ "knowledgeLevel, "
				+ "jobOfferId "
				+ ") values ( "
				+ ":name, "
				+ ":knowledgeLevel, "
				+ ":jobOfferId )";

		try (Connection con = sql2o.beginTransaction()) {
		    long insertedId = (long) con.createQuery(sqlInsertJob, true)
			    .addParameter("name", jobOffer.getName())
			    .addParameter("description", jobOffer.getDescription())
			    .addParameter("minYearlySalary", jobOffer.getYearlySalaryRange().getMinYearlySalary())
			    .addParameter("maxYearlySalary", jobOffer.getYearlySalaryRange().getMaxYearlySalary())
			    .addParameter("workingTime", jobOffer.getWorkingTime().getCode())
			    .addParameter("companyId", jobOffer.getCompany().getId())
			    .executeUpdate()
			    .getKey();
		    
		    jobOffer.setId(insertedId);
		    
		    Query queryForDegrees = con.createQuery(sqlInsertDegree);
		    jobOffer.getRequiredAcademicDegrees().forEach(d -> queryForDegrees
		    		.addParameter("name", d.getName())
		    		.addParameter("jobOfferId", insertedId)
		    		.addToBatch()
		    		);
		    queryForDegrees.executeBatch();
		    
		    Query queryForSkills = con.createQuery(sqlInsertSkill);
		    jobOffer.getRequiredSkills().forEach(s -> queryForSkills
		    		.addParameter("name", s.getName())
		    		.addParameter("knowledgeLevel", s.getKnowledgeLevel())
		    		.addParameter("jobOfferId", insertedId)
		    		.addToBatch()
		    		);
		    queryForSkills.executeBatch();
		    
		    con.commit();
		    
		} catch (Sql2oException ex) {
			logger.error(ex.getMessage());
		}
	}
	
	private List<CompanyDTO> findCompaniesDTO() {
		String sql = "SELECT id, name, contactNumber "
				+ "FROM Companies ";

		try (Connection con = sql2o.open()) {
			return con.createQuery(sql)
					.executeAndFetch(CompanyDTO.class);
		}
		catch (Sql2oException ex) {
			logger.error(ex.getMessage());
			return null;
		}
	}

	private CompanyDTO findCompanyDTOById(long companyId) {
		String sql = "SELECT id, name, contactNumber "
				+ "FROM Companies " 
				+ "WHERE id = :companyId";

		try (Connection con = sql2o.open()) {
			return con.createQuery(sql)
					.addParameter("companyId", companyId)
					.executeAndFetchFirst(CompanyDTO.class);
		}
		catch (Sql2oException ex) {
			logger.error(ex.getMessage());
			return null;
		}
	}

	private List<JobDTO> findCompanyJobOffersDTO(long companyId) {
		String sql = "SELECT id, name, description, minYearlySalary, "
				+ "maxYearlySalary, workingTime, companyId " 
				+ "FROM JobOffers " 
				+ "WHERE companyId = :companyId ";

		try (Connection con = sql2o.open()) {
			return con.createQuery(sql)
					.addParameter("companyId", companyId)
					.executeAndFetch(JobDTO.class);
		}
		catch (Sql2oException ex) {
			logger.error(ex.getMessage());
			return null;
		}
	}
	
	private JobDTO findJobOfferDTO(long jobId) {
		String sql = "SELECT id, name, description, minYearlySalary, " 
				+ "maxYearlySalary, workingTime, companyId " 
				+ "FROM JobOffers " 
				+ "WHERE id = :jobId ";

		try (Connection con = sql2o.open()) {
			return con.createQuery(sql)
					.addParameter("jobId", jobId)
					.executeAndFetchFirst(JobDTO.class);
		}
		catch (Sql2oException ex) {
			logger.error(ex.getMessage());
			return null;
		}
	}
	
	private List<DegreeDTO> findRequiredDegreesJobOfferDTO(long jobId) {
		String sql = "SELECT name " 
				+ "FROM RequiredAcademicDegrees "
				+ "WHERE jobOfferId = :jobId ";

		try (Connection con = sql2o.open()) {
			return con.createQuery(sql)
					.addParameter("jobId", jobId)
					.executeAndFetch(DegreeDTO.class);
		}
		catch (Sql2oException ex) {
			logger.error(ex.getMessage());
			return null;
		}
	}
	
	private List<SkillDTO> findRequiredSkillsJobOfferDTO(long jobId) {
		String sql = "SELECT name, knowledgeLevel " 
				+ "FROM RequiredSkills "
				+ "WHERE jobOfferId = :jobId ";

		try (Connection con = sql2o.open()) {
			return con.createQuery(sql)
					.addParameter("jobId", jobId)
					.executeAndFetch(SkillDTO.class);
		}
		catch (Sql2oException ex) {
			logger.error(ex.getMessage());
			return null;
		}
	}
	
	private List<JobDTO> findJobOffersDTOWhithDegreesAndSkills(long applicantId, List<String> applicantDegreesName, List<String> applicantSkillsName) {
		String sql = "SELECT job.id, job.name, job.description, job.minYearlySalary, " 
			    + "job.maxYearlySalary, job.workingTime, job.companyId  FROM JobOffers job WHERE job.id IN ( "
				+ "SELECT matchedSkills.jobOfferId FROM "
				+ "( "
				+ "SELECT ski.jobOfferId, COUNT(DISTINCT ski.name) AS cnt, "
				+ "SUM(ski.knowledgeLevel) AS sum_req_kn, "
				+ "(SELECT COUNT(DISTINCT name) FROM RequiredSkills WHERE jobOfferId = ski.jobOfferId) AS sum_req_ski "
				+ "FROM RequiredSkills ski "
				+ "WHERE ski.name IN (:applicantSkills) "
				+ "GROUP BY ski.jobOfferId "
				+ ") AS matchedSkills, "
				+ "( "
				+ "SELECT deg.jobOfferId, COUNT(DISTINCT deg.name) AS cnt, "
				+ "(SELECT COUNT(DISTINCT name) FROM RequiredAcademicDegrees WHERE jobOfferId = deg.jobOfferId) AS sum_req_deg "
				+ "FROM RequiredAcademicDegrees deg "
				+ "WHERE deg.name IN (:applicantDegrees) "
				+ "GROUP BY deg.jobOfferId "
				+ ") AS matchedDegrees "
				+ "WHERE matchedSkills.jobOfferId = matchedDegrees.jobOfferId "
				+ "AND matchedDegrees.cnt = matchedDegrees.sum_req_deg "
				+ "AND matchedSkills.cnt = matchedSkills.sum_req_ski "
				+ "AND matchedSkills.sum_req_kn <= (SELECT SUM(appSki.knowledgeLevel) "
				+ "FROM ApplicantSkills appSki "
				+ "WHERE appSki.applicantId = :applicantId "
				+ "AND appSki.name IN (SELECT name "
				+ "FROM RequiredSkills "
				+ "WHERE jobOfferId = matchedSkills.jobOfferId)) "
				+ "ORDER BY matchedSkills.sum_req_kn DESC "
				+ ") ";
		
		try (Connection con = sql2o.open()) {
			
			return con.createQuery(sql)
					.addParameter("applicantSkills", applicantSkillsName)
					.addParameter("applicantDegrees", applicantDegreesName)
					.addParameter("applicantId", applicantId)
					.executeAndFetch(JobDTO.class);
		}
		catch (Sql2oException ex) {
			logger.error(ex.getMessage());
			return null;
		}
	}
	
	private Company mapDTOToCompany(CompanyDTO companyDTO, List<JobDTO> jobsDTO) {
		
		Company company = new Company(companyDTO.id, companyDTO.name, companyDTO.contactNumber, null);
		
		for (JobDTO dto : jobsDTO) {
			List<DegreeDTO> degreesDTO = findRequiredDegreesJobOfferDTO(dto.id);
			List<RequiredAcademicDegree> degrees = new LinkedList<>();
			for (DegreeDTO degreeDTO : degreesDTO) {
				degrees.add(new RequiredAcademicDegree(degreeDTO.name));
			}
			
			List<SkillDTO> skillsDTO = findRequiredSkillsJobOfferDTO(dto.id);
			List<RequiredSkill> skills = new LinkedList<>();
			for (SkillDTO skillDTO : skillsDTO) {
				skills.add(new RequiredSkill(skillDTO.name, skillDTO.knowledgeLevel));
			}
			
			company.addJobOffer(
					dto.id,
					dto.name,
					dto.description,
					dto.minYearlySalary,
					dto.maxYearlySalary,
					WorkingTime.getWorkingTimeFromCode(dto.workingTime).name(),
					degrees, 
					skills);
		}
		
		return company;
	}
}
