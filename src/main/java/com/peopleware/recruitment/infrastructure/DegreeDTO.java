package com.peopleware.recruitment.infrastructure;

public class DegreeDTO {
	String name;
	
	public DegreeDTO(String name) {
		this.name = name;
	}
}
