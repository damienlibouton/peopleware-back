package com.peopleware.recruitment.infrastructure;


public class SkillDTO {
	String name;
	int knowledgeLevel;
	
	public SkillDTO(String name, int knowledgeLevel) {
		this.name = name;
		this.knowledgeLevel = knowledgeLevel;
	}
}
