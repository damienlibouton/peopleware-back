package com.peopleware.recruitment.domain.common;

public enum WorkingTime {
	FULL_TIME(1),
	PART_TIME(2),
	FULL_AND_PART_TIME(3);
	
	private final int code;
	
	WorkingTime(int workingTimeCode) {
		this.code = workingTimeCode;
	}
	
	public int getCode() {
		return this.code;
	}
	
	public static WorkingTime getWorkingTimeFromCode(int code) {
        for (WorkingTime wt : WorkingTime.values()) {
            if (wt.getCode() == code) {
                return wt;
            }
        }
        throw new IllegalArgumentException("The code " + code + "does not exist in the working time enum");
	}
}
