package com.peopleware.recruitment.domain.applicant;

import org.apache.commons.lang3.StringUtils;

public class ApplicantDegree {
	private String name;
	
	public ApplicantDegree(String name) {
		this.setName(name);
	}
	
	public String getName() {
		return this.name;
	}
	
	private void setName(String name) {
		if (StringUtils.isBlank(name)) {
			throw new IllegalArgumentException("The academic degree name of the applicant is null or empty");
		}
		
		if (name.length() > 200) {
			throw new IllegalArgumentException("The academic degree name of the applicant must be 200 characters or less");
		}
		
		this.name = name;
	}
}
