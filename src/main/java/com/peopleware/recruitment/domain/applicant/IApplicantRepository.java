package com.peopleware.recruitment.domain.applicant;

import java.util.List;

public interface IApplicantRepository {
	Applicant findApplicantById(long applicantId);
	
	List<Applicant> findAllApplicants();
	
	List<Applicant> findApplicantsWhithDegreesAndSkills(
			List<String> requiredDegreesName, List<String> requiredSkillsName, int knowledgeLevelOfRequiredSkills);
	
	void AddApplicant(Applicant applicant);
}
