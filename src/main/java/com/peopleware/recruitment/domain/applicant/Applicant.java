package com.peopleware.recruitment.domain.applicant;

import java.util.LinkedList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;

import com.peopleware.recruitment.domain.common.WorkingTime;
import com.peopleware.recruitment.domain.seedwork.Entity;

public class Applicant extends Entity {
	private String fullName;
	private String contactNumber;
	private String email;
	private int monthlyMinSalary;
	private List<ApplicantDegree> academicDegrees;
	private List<ApplicantSkill> skills;
	private WorkingTime workingTime;
	
	public Applicant(long id, String fullName, String contactNumber, String email, int monthlyMinSalary,
			String workingTime, List<ApplicantDegree> academicDegrees,
			List<ApplicantSkill> skills) {
		super(id);
		this.setFullName(fullName);
		this.setContactNumber(contactNumber);
		this.setEmail(email);
		this.setMonthlyMinSalary(monthlyMinSalary);
		this.setWorkingTime(workingTime);
		this.setAcademicDegrees(academicDegrees);
		this.setSkills(skills);
	}
	
	public Applicant(String fullName, String contactNumber, String email, int monthlyMinSalary,
			String workingTime, List<ApplicantDegree> academicDegrees,
			List<ApplicantSkill> skills) {
		super();
		this.setFullName(fullName);
		this.setContactNumber(contactNumber);
		this.setEmail(email);
		this.setMonthlyMinSalary(monthlyMinSalary);
		this.setWorkingTime(workingTime);
		this.setAcademicDegrees(academicDegrees);
		this.setSkills(skills);
	}
	
	public ApplicantDegree addAcademicDegree(String name) {
		
		for (ApplicantDegree existingAcademicDegree : this.academicDegrees) {
			if (existingAcademicDegree.getName().equalsIgnoreCase(name)) {
				return existingAcademicDegree;
			}
		}
		
		ApplicantDegree degree = new ApplicantDegree(name);
		this.academicDegrees.add(degree);
		
		return degree;
	}
	
	public ApplicantSkill addSkill(String name, int knowledgeLevel) {
		
		for (ApplicantSkill existingSkill : this.skills) {
			if (existingSkill.getName().equalsIgnoreCase(name)) {
				existingSkill.updateKnowledgeLevel(knowledgeLevel);
				return existingSkill;
			}
		}
		
		ApplicantSkill skill = new ApplicantSkill(name, knowledgeLevel);
		this.skills.add(skill);
		
		return skill;
	}
	
	public int getKnowledgeLevelOfRequiredSkills(List<ApplicantSkill> requiredSkills) {
		int sum = 0;
		
		for (ApplicantSkill requiredSkill : requiredSkills) {
			for (ApplicantSkill applicantSkill : this.skills) {
				if (applicantSkill.getName().equalsIgnoreCase(requiredSkill.getName())) {
					sum += applicantSkill.getKnowledgeLevel();
				}
			}
		}
		
		return sum;
	}
	

	public String getFullName() {
		return this.fullName;
	}
	
	public String getContactNumber() {
		return this.contactNumber;
	}
	
	public String getEmail() {
		return this.email;
	}
	
	public int getMonthlyMinSalary() {
		return this.monthlyMinSalary;
	}
	
	public WorkingTime getWorkingTime() {
		return this.workingTime;
	}
	
	public List<ApplicantDegree> getAcademicDegrees() {
		return this.academicDegrees;
	}
	
	public List<ApplicantSkill> getSkills() {
		return this.skills;
	}
	
	private void setFullName(String fullName) {
		if (StringUtils.isBlank(fullName)) {
			throw new IllegalArgumentException("The full name of the applicant is null or empty");
		}
		
		if (fullName.length() > 100) {
			throw new IllegalArgumentException("The full name of the applicant must be 100 characters or less");
		}
		
		this.fullName = fullName;
	}
	
	private void setContactNumber(String contactNumber) {
		if (StringUtils.isBlank(contactNumber)) {
			throw new IllegalArgumentException("The contact number of the applicant is null or empty");
		}
		
		if (contactNumber.length() > 50) {
			throw new IllegalArgumentException("The contact number of the applicant must be 50 characters or less");
		}
		
		this.contactNumber = contactNumber;
	}
	
	private void setEmail(String email) {
		if (StringUtils.isBlank(email)) {
			throw new IllegalArgumentException("The contact number of the applicant is null or empty");
		}
		
		if (email.length() > 200) {
			throw new IllegalArgumentException("The email of the applicant must be 200 characters or less");
		}
		
		this.email = email;
	}
	
	private void setMonthlyMinSalary(int monthlyMinSalary) {
		if (monthlyMinSalary <= 0) {
			throw new IllegalArgumentException("The monthly minimum salary (" + monthlyMinSalary +
					") of the applicant (" + this.getFullName() + ") is inferior or equal to 0");
		}
		
		this.monthlyMinSalary = monthlyMinSalary;
	}
	
	private void setWorkingTime(String workingTime) {
		if (StringUtils.isBlank(workingTime)) {
			throw new IllegalArgumentException("The working time of the applicant is null or empty");
		}
		
		try {
			this.workingTime = WorkingTime.valueOf(workingTime.toUpperCase());
		}
		catch (Exception e) {
			throw new IllegalArgumentException("The working time of the applicant (" + workingTime +
					") does not exist\n" + e.getStackTrace());
		}
	}
	
	private void setAcademicDegrees(List<ApplicantDegree> academicDegrees) {
		if (academicDegrees == null) {
			this.academicDegrees = new LinkedList<ApplicantDegree>();
		}
		else {
			this.academicDegrees = academicDegrees;
		}
	}
	
	private void setSkills(List<ApplicantSkill> skills) {
		if (skills == null || skills.size() == 0) {
			throw new IllegalArgumentException("The skills of the applicant are null or empty");
		}
		else {
			this.skills = skills;
		}
	}
}
