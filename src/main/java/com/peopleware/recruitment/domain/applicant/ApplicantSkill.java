package com.peopleware.recruitment.domain.applicant;


import org.apache.commons.lang3.StringUtils;

public class ApplicantSkill {
	private String name;
	private int knowledgeLevel;

	public ApplicantSkill(String name, int knowledgeLevel) {
		this.setName(name);
		this.updateKnowledgeLevel(knowledgeLevel);
	}

	public String getName() {
		return this.name;
	}

	public int getKnowledgeLevel() {
		return this.knowledgeLevel;
	}
	
	public void updateKnowledgeLevel(int knowledgeLevel) {
		if (knowledgeLevel <= 0 || knowledgeLevel > 5) {
			throw new IllegalArgumentException("The knowledgeLevel of the applicant skill is outside of the scale 1-5");
		}
		this.knowledgeLevel = knowledgeLevel;
	}

	private void setName(String name) {
		if (StringUtils.isBlank(name)) {
			throw new IllegalArgumentException("The name of the applicant skill is null or empty");
		}

		if (name.length() > 100) {
			throw new IllegalArgumentException("The name of the applicant skill must be 100 characters or less");
		}

		this.name = name;
	}
}
