package com.peopleware.recruitment.domain.company;

import java.util.LinkedList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;

import com.peopleware.recruitment.domain.common.WorkingTime;
import com.peopleware.recruitment.domain.seedwork.Entity;

public class JobOffer extends Entity {
	private String name;
	private String description;
	private YearlySalaryRange yearlySalaryRange;
	private WorkingTime workingTime;
	private List<RequiredAcademicDegree> requiredAcademicDegrees;
	private List<RequiredSkill> requiredSkills;
	private Company company;
	
	public JobOffer(String name, String description, int minYearlySalary, int maxYearlySalary,
			String workingTime, List<RequiredAcademicDegree> requiredAcademicDegrees,
			List<RequiredSkill> requiredSkills, Company company) {
		super();
		this.setName(name);
		this.setDescription(description);
		this.updateYearlySalaryRange(minYearlySalary, maxYearlySalary);
		this.setWorkingTime(workingTime);
		this.setRequiredAcademicDegrees(requiredAcademicDegrees);
		this.setRequiredSkills(requiredSkills);
		this.setCompany(company);
	}
	
	public JobOffer(long id, String name, String description, int minYearlySalary, int maxYearlySalary,
			String workingTime, List<RequiredAcademicDegree> requiredAcademicDegrees,
			List<RequiredSkill> requiredSkills, Company company) {
		super(id);
		this.setName(name);
		this.setDescription(description);
		this.updateYearlySalaryRange(minYearlySalary, maxYearlySalary);
		this.setWorkingTime(workingTime);
		this.setRequiredAcademicDegrees(requiredAcademicDegrees);
		this.setRequiredSkills(requiredSkills);
		this.setCompany(company);
	}
	
	public void updateYearlySalaryRange(int minYearlySalary, int maxYearlySalary) {
		if (this.yearlySalaryRange == null) {
			this.yearlySalaryRange = new YearlySalaryRange(minYearlySalary, maxYearlySalary);
		}
		else {
			this.yearlySalaryRange.updateYearlySalaryRange(minYearlySalary, maxYearlySalary);
		}
	}
	
	public String getName() {
		return this.name;
	}
	
	public String getDescription() {
		return this.description;
	}
	
	public YearlySalaryRange getYearlySalaryRange() {
		return this.yearlySalaryRange;
	}
	
	public WorkingTime getWorkingTime() {
		return this.workingTime;
	}
	
	public List<RequiredAcademicDegree> getRequiredAcademicDegrees() {
		return this.requiredAcademicDegrees;
	}
	
	public List<RequiredSkill> getRequiredSkills() {
		return this.requiredSkills;
	}
	
	public Company getCompany() {
		return this.company;
	}
	
	private void setName(String name) {
		if (StringUtils.isBlank(name)) {
			throw new IllegalArgumentException("The name of the job is null or empty");
		}
		
		if (name.length() > 100) {
			throw new IllegalArgumentException("The name of the job must be 100 characters or less");
		}
		
		this.name = name;
	}
	
	private void setDescription(String description) {
		if (StringUtils.isBlank(description)) {
			throw new IllegalArgumentException("The description of the job is null or empty");
		}
		
		if (description.length() > 500) {
			throw new IllegalArgumentException("The description of the job must be 500 characters or less");
		}
		
		this.description = description;
	}
	
	private void setWorkingTime(String workingTime) {
		if (StringUtils.isBlank(workingTime)) {
			throw new IllegalArgumentException("The working time of the job offer is null or empty");
		}
		
		try {
			this.workingTime = WorkingTime.valueOf(workingTime.toUpperCase());
		}
		catch (Exception e) {
			throw new IllegalArgumentException("The working time of the job offer (" + workingTime +
					") does not exist\n" + e.getStackTrace());
		}
		
		if (this.workingTime == WorkingTime.FULL_AND_PART_TIME) {
			this.workingTime = null;
			throw new IllegalArgumentException("The working time of the job offer (" + workingTime +
					") is not allowed");
		}
	}
	
	private void setRequiredAcademicDegrees(List<RequiredAcademicDegree> requiredAcademicDegrees) {
		if (requiredAcademicDegrees == null) {
			this.requiredAcademicDegrees = new LinkedList<RequiredAcademicDegree>();
		}
		else {
			this.requiredAcademicDegrees = requiredAcademicDegrees;
		}
	}
	
	private void setRequiredSkills(List<RequiredSkill> requiredSkills) {
		if (requiredSkills == null || requiredSkills.size() == 0) {
			throw new IllegalArgumentException("The required skills of the job are null or empty");
		}
		else {
			this.requiredSkills = requiredSkills;
		}
	}
	
	private void setCompany(Company company) {
		if (company == null) {
			throw new IllegalArgumentException("The company of the job is null or empty");
		}
		
		this.company = company;
	}
}
