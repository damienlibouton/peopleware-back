package com.peopleware.recruitment.domain.company;

import java.util.LinkedList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;

import com.peopleware.recruitment.domain.seedwork.Entity;

public class Company extends Entity {
	
	private String name;
	private String contactNumber;
	private List<JobOffer> jobs;
	
	public Company(long id, String name, String contactNumber, List<JobOffer> jobs) {
		super(id);
		this.setName(name);
		this.setContactNumber(contactNumber);
		this.setJobs(jobs);
	}
	
	public Company(String name, String contactNumber, List<JobOffer> jobs) {
		super();
		this.setName(name);
		this.setContactNumber(contactNumber);
		this.setJobs(jobs);
	}
	
	public JobOffer addJobOffer(long id, String name, String description, int minYearlySalary, int maxYearlySalary,
			String workingTime, List<RequiredAcademicDegree> requiredAcademicDegrees,
			List<RequiredSkill> requiredSkills) {
		
		for (JobOffer existingJob : this.jobs) {
			if (existingJob.getName().equalsIgnoreCase(name)) {
				return existingJob;
			}
		}
		
		JobOffer job = new JobOffer(id, name, description, minYearlySalary, maxYearlySalary,
				workingTime, requiredAcademicDegrees, requiredSkills, this);
		
		this.jobs.add(job);
		
		return job;
	}
	
	public String getName() {
		return this.name;
	}
	
	public String getContactNumber() {
		return this.contactNumber;
	}
	
	public List<JobOffer> getJobs() {
		return this.jobs;
	}
	
	public JobOffer getJob(String name) {
		for (JobOffer job : this.jobs) {
			if (job.getName().equalsIgnoreCase(name)) {
				return job;
			}
		}
		return null;
	}
	
	public JobOffer getJob(long jobId) {
		for (JobOffer job : this.jobs) {
			if (job.getId() == jobId) {
				return job;
			}
		}
		return null;
	}
	
	private void setName(String name) {
		if (StringUtils.isBlank(name)) {
			throw new IllegalArgumentException("The name of the company is null or empty");
		}
		
		if (name.length() > 100) {
			throw new IllegalArgumentException("The name of the company must be 100 characters or less");
		}
		
		this.name = name;
	}
	
	private void setContactNumber(String contactNumber) {
		if (StringUtils.isBlank(contactNumber)) {
			throw new IllegalArgumentException("The contact number of the company is null or empty");
		}
		
		if (!java.util.regex.Pattern.matches(
				"^((\\+|00)32\\s?|0)(\\d\\s?\\d{3}|\\d{2}\\s?\\d{2})(\\s?\\d{2}){2}$",
				contactNumber)) {
			throw new IllegalArgumentException("The contact number format of the company is invalid");
		}
		
		this.contactNumber = contactNumber;
	}
	
	private void setJobs(List<JobOffer> jobs) {
		if (jobs == null) {
			this.jobs = new LinkedList<JobOffer>();
		}
		else {
			this.jobs = jobs;
		}
	}
}
