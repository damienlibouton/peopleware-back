package com.peopleware.recruitment.domain.company;

public class YearlySalaryRange {
	private int minYearlySalary;
	private int maxYearlySalary;
	
	public YearlySalaryRange(int minYearlySalary, int maxYearlySalary) {
		this.updateYearlySalaryRange(minYearlySalary, maxYearlySalary);
	}
	
	public int getMinYearlySalary() {
		return minYearlySalary;
	}
	
	public int getMaxYearlySalary() {
		return maxYearlySalary;
	}
	
	public void updateYearlySalaryRange(int minYearlySalary, int maxYearlySalary) {
		if (minYearlySalary < 0) {
			throw new IllegalArgumentException("The minimum of the yearly salary (" + minYearlySalary +
					") is inferior to 0");
		}
		
		if (maxYearlySalary < 0) {
			throw new IllegalArgumentException("The maximum of the yearly salary (" + maxYearlySalary +
					") is inferior to 0");
		}
		
		if (minYearlySalary > maxYearlySalary) {
			throw new IllegalArgumentException("The minimum of the yearly salary (" + minYearlySalary +
					") is superior to maximum (" + maxYearlySalary + ") of the yearly salary");
		}
		
		this.minYearlySalary = minYearlySalary;
		this.maxYearlySalary = maxYearlySalary;
	}
}
