package com.peopleware.recruitment.domain.company;

import org.apache.commons.lang3.StringUtils;

public class RequiredAcademicDegree {
	private String name;
	
	public RequiredAcademicDegree(String name) {
		this.setName(name);
	}
	
	public String getName() {
		return this.name;
	}
	
	private void setName(String name) {
		if (StringUtils.isBlank(name)) {
			throw new IllegalArgumentException("The academic degree name of the job offer is null or empty");
		}
		
		if (name.length() > 200) {
			throw new IllegalArgumentException("The academic degree name of the job offer must be 200 characters or less");
		}
		
		this.name = name;
	}
}
