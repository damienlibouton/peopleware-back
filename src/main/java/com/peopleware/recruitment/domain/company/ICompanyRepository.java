package com.peopleware.recruitment.domain.company;

import java.util.List;

public interface ICompanyRepository {
	List<Company> findAllCompanies();
	
	Company findCompanyById(long id);
	
	List<JobOffer> findAllJobOffers();
	
	List<JobOffer> findCompanyJobOffers(long companyId);
	
	List<JobOffer> findJobOffersWhithDegreesAndSkills(long applicantId,
			List<String> applicantDegreesName, List<String> applicantSkillsName);
	
	List<RequiredAcademicDegree> findRequiredAcademicDegreeOfJobOffer(long jobId);
	
	List<RequiredSkill> findRequiredSkillsOfJobOffer(long jobId);
	
	JobOffer findJobOfferById(long jobId);
	
	void AddJobOffer(JobOffer jobOffer);
}
