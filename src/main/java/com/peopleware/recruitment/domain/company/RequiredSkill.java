package com.peopleware.recruitment.domain.company;

import org.apache.commons.lang3.StringUtils;

public class RequiredSkill {
	private String name;
	private int knowledgeLevel;
	
	public RequiredSkill(String name, int knowledgeLevel) {
		this.setName(name);
		this.setKnowledgeLevel(knowledgeLevel);
	}
	
	public String getName() {
		return this.name;
	}
	
	public int getKnowledgeLevel() {
		return this.knowledgeLevel;
	}
	
	private void setName(String name) {
		if (StringUtils.isBlank(name)) {
			throw new IllegalArgumentException("The name of the job offer skill is null or empty");
		}
		
		if (name.length() > 100) {
			throw new IllegalArgumentException("The name of the job offer skill must be 100 characters or less");
		}
		
		this.name = name;
	}
	
	private void setKnowledgeLevel(int knowledgeLevel) {
		if (knowledgeLevel <= 0 || knowledgeLevel > 5) {
			throw new IllegalArgumentException("The knowledgeLevel of the job offer skill is outside of the scale 1-5");
		}
		this.knowledgeLevel = knowledgeLevel;
	}
}
