package com.peopleware.recruitment.domain.seedwork;


public class Entity {
	private long id;
	
	public Entity() {
		this.setId(-1L);
	}
	
	public Entity(long id) {
		this.setId(id);
	}
	
	public long getId() {
		return this.id;
	}
	
	public void setId(long id) {
		this.id = id;
	}
	
	public boolean isTransient() {
		return this.id == -1;
	}
	
	

}
