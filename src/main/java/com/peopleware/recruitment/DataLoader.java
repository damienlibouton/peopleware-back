package com.peopleware.recruitment;

import java.util.LinkedList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import com.peopleware.recruitment.domain.applicant.Applicant;
import com.peopleware.recruitment.domain.applicant.ApplicantDegree;
import com.peopleware.recruitment.domain.applicant.ApplicantSkill;
import com.peopleware.recruitment.domain.applicant.IApplicantRepository;
import com.peopleware.recruitment.domain.common.WorkingTime;
import com.peopleware.recruitment.domain.company.Company;
import com.peopleware.recruitment.domain.company.ICompanyRepository;
import com.peopleware.recruitment.domain.company.JobOffer;
import com.peopleware.recruitment.domain.company.RequiredAcademicDegree;
import com.peopleware.recruitment.domain.company.RequiredSkill;
 
@Component
public class DataLoader implements CommandLineRunner {
 
    private final Logger logger = LoggerFactory.getLogger(DataLoader.class);
    
    private ICompanyRepository companyRepository;
    private IApplicantRepository applicantRepository;
    
    public DataLoader(ICompanyRepository companyRepository, IApplicantRepository applicantRepository) {
    	this.companyRepository = companyRepository;
    	this.applicantRepository = applicantRepository;
    }
 
    @Override
    public void run(String... strings) throws Exception {
        logger.info("Loading data...");
              
        Company company = companyRepository.findCompanyById(2L);
        
        List<RequiredAcademicDegree> degrees = new LinkedList<>();
        degrees.add(new RequiredAcademicDegree("Master of Mathematics"));
        
        List<RequiredSkill> skills = new LinkedList<>();
        skills.add(new RequiredSkill("Python", 4));
        
        
        JobOffer job = new JobOffer("Data Scientist", "Data scientist with insights in the latest trends", 40000, 80000, "FULL_TIME", degrees, skills, company);
        companyRepository.AddJobOffer(job);
        logger.info("JobId : " + job.getId());
        
        degrees.clear();
        degrees.add(new RequiredAcademicDegree("Master of Computer Science"));
        
        skills.add(new RequiredSkill("SQL", 3));
        
        job = new JobOffer("Python Developer", "Python developer with insights in the latest trends", 50000, 80000, "FULL_TIME", degrees, skills, company);
        companyRepository.AddJobOffer(job);
        logger.info("JobId : " + job.getId());
        
        List<ApplicantDegree> appDegrees = new LinkedList<>();
        appDegrees.add(new ApplicantDegree("Master of Computer Science"));
        appDegrees.add(new ApplicantDegree("Master of Mathematics"));
        
        List<ApplicantSkill> appSkills = new LinkedList<>();
        appSkills.add(new ApplicantSkill("Python", 4));
        appSkills.add(new ApplicantSkill("SQL", 3));
        
        Applicant app = new Applicant("Martin Boo", "0489 99 90 76", "mab@mojj.mo", 3999, WorkingTime.PART_TIME.name(), appDegrees, appSkills);
        applicantRepository.AddApplicant(app);
        logger.info("Applicant id : " + app.getId());
        
        appDegrees.clear();
        appDegrees.add(new ApplicantDegree("Master of Computer Science"));
        
        appSkills.clear();
        appSkills.add(new ApplicantSkill("C#", 4));
        appSkills.add(new ApplicantSkill("SQL", 4));
        appSkills.add(new ApplicantSkill("Angular", 4));
        
        app = new Applicant("Celine Sharp", "0489 99 90 76", "csh@mojj.mo", 3000, WorkingTime.PART_TIME.name(), appDegrees, appSkills);
        applicantRepository.AddApplicant(app);
        logger.info("Applicant id : " + app.getId());
    }
}