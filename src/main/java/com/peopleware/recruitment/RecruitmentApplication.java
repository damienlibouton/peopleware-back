package com.peopleware.recruitment;

import javax.sql.DataSource;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.web.context.annotation.ApplicationScope;
import org.sql2o.Sql2o;

import com.peopleware.recruitment.application.ApplicantCommandsAndQueries;
import com.peopleware.recruitment.application.JobOfferCommandsAndQueries;
import com.peopleware.recruitment.domain.applicant.IApplicantRepository;
import com.peopleware.recruitment.domain.company.ICompanyRepository;
import com.peopleware.recruitment.infrastructure.ApplicantRepositoryH2;
import com.peopleware.recruitment.infrastructure.CompanyRepositoryH2;

@SpringBootApplication
public class RecruitmentApplication {

	public static void main(String[] args) {
		SpringApplication.run(RecruitmentApplication.class, args);
	}
	
	@Bean
	@ApplicationScope
	Sql2o sql2o(DataSource dataSource) {
		return new Sql2o(dataSource);
	}
	
	@Bean
	ICompanyRepository companyRepository(Sql2o sql2o) {
		return new CompanyRepositoryH2(sql2o);
	}
	
	@Bean
	IApplicantRepository applicantRepository(Sql2o sql2o) {
		return new ApplicantRepositoryH2(sql2o);
	}
	
	@Bean
	JobOfferCommandsAndQueries jobOfferCQ(ICompanyRepository companyRepository, IApplicantRepository applicantRepository) {
		return new JobOfferCommandsAndQueries(companyRepository, applicantRepository);
	}
	
	@Bean
	ApplicantCommandsAndQueries applicantCQ(ICompanyRepository companyRepository, IApplicantRepository applicantRepository) {
		return new ApplicantCommandsAndQueries(companyRepository, applicantRepository);
	}
}

