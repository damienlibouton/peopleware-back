DROP TABLE IF EXISTS ApplicantDegrees;
DROP TABLE IF EXISTS ApplicantSkills;
DROP TABLE IF EXISTS Applicants;

DROP TABLE IF EXISTS RequiredAcademicDegrees;
DROP TABLE IF EXISTS RequiredSkills;
DROP TABLE IF EXISTS JobOffers;
DROP TABLE IF EXISTS Companies;

CREATE TABLE IF NOT EXISTS Applicants (
  id IDENTITY NOT NULL PRIMARY KEY,
  fullName VARCHAR(100) NOT NULL,
  contactNumber VARCHAR(50) NOT NULL,
  email VARCHAR(200) NOT NULL,
  monthlyMinSalary INT NOT NULL,
  workingTime TINYINT NOT NULL
);

CREATE TABLE IF NOT EXISTS ApplicantDegrees (
  name VARCHAR(200) NOT NULL,
  applicantId BIGINT,
  FOREIGN KEY (applicantId) REFERENCES Applicants(id)
);

CREATE TABLE IF NOT EXISTS ApplicantSkills (
  name VARCHAR(100) NOT NULL,
  knowledgeLevel TINYINT NOT NULL,
  applicantId BIGINT,
  FOREIGN KEY (applicantId) REFERENCES Applicants(id)
);

CREATE TABLE IF NOT EXISTS Companies (
  id IDENTITY NOT NULL PRIMARY KEY,
  name VARCHAR(100) NOT NULL,
  contactNumber VARCHAR(50) NOT NULL
);

CREATE TABLE IF NOT EXISTS JobOffers (
  id IDENTITY NOT NULL PRIMARY KEY,
  name VARCHAR(100) NOT NULL,
  description VARCHAR(500) NOT NULL,
  minYearlySalary INT NOT NULL,
  maxYearlySalary INT NOT NULL,
  workingTime TINYINT NOT NULL,
  companyId BIGINT,
  FOREIGN KEY (companyId) REFERENCES Companies(id)
);

CREATE TABLE IF NOT EXISTS RequiredAcademicDegrees (
  name VARCHAR(200) NOT NULL,
  jobOfferId BIGINT,
  FOREIGN KEY (jobOfferId) REFERENCES JobOffers(id)
);

CREATE TABLE IF NOT EXISTS RequiredSkills (
  name VARCHAR(100) NOT NULL,
  knowledgeLevel TINYINT NOT NULL,
  jobOfferId BIGINT,
  FOREIGN KEY (jobOfferId) REFERENCES JobOffers(id)
);

--------------------APPLICANTS--------------------
INSERT INTO Applicants VALUES(default, 'John Doe', '023456789', 'jod@vb.com', 3333, 2);
INSERT INTO Applicants VALUES(default, 'Marie Monet', '0498324156', 'mam@ipo.com', 4444, 3);
INSERT INTO Applicants VALUES(default, 'Nicolas Lambert', '+3281976584', 'nil@aze.com', 2222, 1);
INSERT INTO Applicants VALUES(default, 'Justine Katz', '0032487765490', 'juk@klm.com', 4321, 1);

INSERT INTO ApplicantDegrees VALUES('Bachelor of Science in Engineering', 1);
INSERT INTO ApplicantDegrees VALUES('Master of Computer Science', 1);
INSERT INTO ApplicantDegrees VALUES('Master of Mathematics', 2);
INSERT INTO ApplicantDegrees VALUES('Master of History', 3);
INSERT INTO ApplicantDegrees VALUES('Master of Computer Science', 3);
INSERT INTO ApplicantDegrees VALUES('Master of Mathematics', 3);
INSERT INTO ApplicantDegrees VALUES('Master of Computer Science', 4);

--John Doe
INSERT INTO ApplicantSkills VALUES('Java', 5, 1);
INSERT INTO ApplicantSkills VALUES('SQL', 3, 1);

--Marie Monnet
INSERT INTO ApplicantSkills VALUES('Java', 5, 2);
INSERT INTO ApplicantSkills VALUES('SQL', 5, 2);
INSERT INTO ApplicantSkills VALUES('Bootsrap', 5, 2);
INSERT INTO ApplicantSkills VALUES('Angular', 5, 2);
INSERT INTO ApplicantSkills VALUES('Python', 5, 2);

--Nicolas Lambert
INSERT INTO ApplicantSkills VALUES('Java', 3, 3);
INSERT INTO ApplicantSkills VALUES('Bootstrap', 3, 3);

--Justine Katz
INSERT INTO ApplicantSkills VALUES('Java', 4, 4);
INSERT INTO ApplicantSkills VALUES('SQL', 5, 4);
INSERT INTO ApplicantSkills VALUES('Bootstrap', 1, 4);


--------------------COMPANIES--------------------
INSERT INTO Companies VALUES(default, 'BizBizTalk', '028769087');
INSERT INTO Companies VALUES(default, 'BulletPoint', '+3271675478');
INSERT INTO Companies VALUES(default, 'LifeCycle', '003210739820');
INSERT INTO Companies VALUES(default, 'ShareMoney', '003204896788');

--BizBizTalk
INSERT INTO JobOffers VALUES(default, 'Java Software developer', 'Java developer with insights in the latest trends of web development', 30000, 40000, 1, 1);
INSERT INTO RequiredAcademicDegrees VALUES ('Master of Computer Science', 1);
INSERT INTO RequiredSkills VALUES ('Java', 4, 1);
INSERT INTO RequiredSkills VALUES ('SQL', 4, 1);
INSERT INTO RequiredSkills VALUES ('Angular', 3, 1);

INSERT INTO JobOffers VALUES(default, 'C# Software developer', 'C# developer with insights in the latest trends of web development', 30000, 40000, 1, 1);
INSERT INTO RequiredAcademicDegrees VALUES ('Master of Computer Science', 2);
INSERT INTO RequiredSkills VALUES ('C#', 4, 2);
INSERT INTO RequiredSkills VALUES ('SQL', 4, 2);
INSERT INTO RequiredSkills VALUES ('Angular', 3, 2);

--BulletPoint
INSERT INTO JobOffers VALUES(default, 'Bootstrap Software developer', 'Bootstrap developer with insights in the latest trends of web development', 30000, 40000, 1, 2);
INSERT INTO RequiredAcademicDegrees VALUES ('Master of Computer Science', 3);
INSERT INTO RequiredSkills VALUES ('Java', 3, 3);
INSERT INTO RequiredSkills VALUES ('SQL', 1, 3);
INSERT INTO RequiredSkills VALUES ('Bootstrap', 5, 3);

--LifeCycle
INSERT INTO JobOffers VALUES(default, 'Angular Software developer', 'Angular developer with insights in the latest trends of web development', 20000, 50000, 1, 3);
INSERT INTO RequiredAcademicDegrees VALUES ('Master of Computer Science', 4);
INSERT INTO RequiredSkills VALUES ('Angular', 3, 4);

--ShareMoney
INSERT INTO JobOffers VALUES(default, 'Junior Java Software developer', 'Junior java developer with insights in the latest trends of web development', 20000, 50000, 1, 4);
INSERT INTO RequiredAcademicDegrees VALUES ('Master of Computer Science', 5);
INSERT INTO RequiredSkills VALUES ('Java', 2, 5);


