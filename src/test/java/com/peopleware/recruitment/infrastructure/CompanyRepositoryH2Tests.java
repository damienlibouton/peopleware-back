package com.peopleware.recruitment.infrastructure;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;

import java.util.LinkedList;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.peopleware.recruitment.domain.common.WorkingTime;
import com.peopleware.recruitment.domain.company.Company;
import com.peopleware.recruitment.domain.company.ICompanyRepository;
import com.peopleware.recruitment.domain.company.JobOffer;
import com.peopleware.recruitment.domain.company.RequiredAcademicDegree;
import com.peopleware.recruitment.domain.company.RequiredSkill;

@RunWith(SpringRunner.class)
@SpringBootTest
public class CompanyRepositoryH2Tests {
	@Autowired
	private ICompanyRepository companyRepository;

	@Test
	public void findCompanyShouldSucces() {
		// Arrange
		long companyId = 1L;
		
		//Affect
		Company company = companyRepository.findCompanyById(companyId);
		
		//Assert
		assertNotNull(company);
		assertNotNull(company.getJobs());
	}
	
	@Test
	public void addJobOfferShouldSuccess() {
		// Arrange
		long companyId = 1L;
		Company company = companyRepository.findCompanyById(companyId);
		List<RequiredAcademicDegree> degrees = new LinkedList<>();
        degrees.add(new RequiredAcademicDegree("Master of Computer Science"));
        degrees.add(new RequiredAcademicDegree("Master of Mathematics"));
        
        List<RequiredSkill> skills = new LinkedList<>();
        skills.add(new RequiredSkill("Python", 4));
        skills.add(new RequiredSkill("SQL", 3));
		
        JobOffer job = company.addJobOffer(
				-1L, 
				"testJob", 
				"testJobDesc", 
				1, 10, 
				WorkingTime.FULL_TIME.name(),
				degrees, skills);
        
		//Affect
		companyRepository.AddJobOffer(job);
		
		//Assert
		assertNotNull(job);
		assertNotEquals(-1L, job.getId());
		assertEquals("testJob", job.getName());
		assertNotNull(company.getJob("testJob"));
	}
}
