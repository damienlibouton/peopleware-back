package com.peopleware.recruitment.application;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertNotNull;

import java.util.LinkedList;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class ApplicantCommandsAndQueriesTests {
	@Autowired
	private ApplicantCommandsAndQueries applicantCQ;
	
	@Test
	public void getAllApplicantsShouldNotBeNull() {
		//Arrange
		List<ApplicantDTO> applicantsDTO = new LinkedList<>();
		
		//Affect
		applicantsDTO = applicantCQ.getAllApplicants();
		
		//Assert
		assertNotNull(applicantsDTO);
	}
	
	@Test
	public void getApplicantsForAJobOfferShouldMatchAtLeastOneApplicant() {
		//Arrange
		List<ApplicantDTO> applicantsDTO = new LinkedList<>();
		
		//Affect
		applicantsDTO = applicantCQ.getApplicantsForAJobOffer(5L);
		
		//Assert
		assertNotNull(applicantsDTO);
		assertThat(applicantsDTO.size()).isGreaterThan(0);
	}

}
