package com.peopleware.recruitment.application;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;

import java.util.LinkedList;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class JobOfferCommandsAndQueriesTests {
	@Autowired
	private JobOfferCommandsAndQueries jobCQ;
	
	@Test
	public void getCompanyIdShoulNotBe0() {
		//Arrange
		CompanyDTO companyDTO = null;
		
		//Affect
		companyDTO = jobCQ.getCompany(1L);
		
		//Assert
		assertNotEquals(0, companyDTO.id);
	}
	
	@Test
	public void getApplicantsForAJobOfferShouldMatchAtLeastOneApplicant() {
		//Arrange
		List<JobDTO> jobsDTO = new LinkedList<>();
		
		//Affect
		jobsDTO = jobCQ.getJobsMatchingDegreesAndSkillsOfApplicant(4L);
		
		//Assert
		assertNotNull(jobsDTO);
		assertThat(jobsDTO.size()).isGreaterThan(0);
	}
	
}
